<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class TokenController extends Controller
{
    public function getToken(Request $request)
    {
        $token = User::select('api_token')->where('email', $request->email)->first();
        //dd($token->api_token);
        if($token['api_token'] != null){
            return response()->json(['status' => 'success', 'token' => $token], 200);
        }else{
            return response()->json(['status' => 'error', 'message' => 'Token not found'], 401);
        }
        
    }
}
