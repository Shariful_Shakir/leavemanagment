<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/users', 'UserController@getUser');

$router->post('/register', 'AuthController@register');
$router->post('/login', 'AuthController@login');
$router->get('/logout', 'AuthController@logout');


$router->get('/employee', 'EmployeeController@getEmployee');
$router->post('/add-employee', 'EmployeeController@addEmployee');
$router->post('/update-employee', 'EmployeeController@updateEmployee');
$router->post('/delete-employee', 'EmployeeController@deleteEmployee');


$router->get('/get-token', 'TokenController@getToken');