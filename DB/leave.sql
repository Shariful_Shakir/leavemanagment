-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Feb 27, 2020 at 06:15 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leave`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `contact_no`, `email`, `address`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Shariful Islam', '2', '01773592770', 'shariful@gmail.com', 'Dimla Nilphamari', 'pCFvsxgBD6.jpg', '1', '2019-12-19 08:41:05', '2019-12-19 09:09:12'),
(2, 'Shariful Islam', '2', '01773592770', 'shariful@gmail.com', 'Dimla Nilphamari', 'pUXd3qP4kU.jpg', '0', '2019-12-19 08:41:05', '2019-12-19 08:47:44'),
(3, 'ioiyutyre', '2', '675433', 'ytrewe', 'hgbfvd', 'MggSMqwVw8.jpg', '0', '2019-12-19 08:50:13', '2019-12-19 08:53:50'),
(4, 'khjghg', '2', '765432', 'kjhghf', 'jyhgfd', 'NVEPg4q7aw.jpg', '0', '2019-12-19 08:54:12', '2019-12-19 09:00:08'),
(5, 'fgdfsds', '1', '5432', 'ghffr', 'ghfdfd', '3DiVBAKFEh.jpg', '0', '2019-12-19 09:00:32', '2019-12-19 09:00:37'),
(6, 'ghfbgdfd', '1', '6754343', 'fds@fd.gdfs', 'mhjghgref', 'x1uab13TmC.jpg', '1', '2019-12-22 06:34:33', '2019-12-22 06:34:33'),
(7, 'ghfbgdfd', '1', '6754343', 'fds@fd.gdfs', 'mhjghgref', 'y9DaBaAspW.jpg', '0', '2019-12-22 06:34:33', '2019-12-22 06:46:41'),
(8, 'ljkhjgh', '1', '876543', 'rtr@3rt.fd', 'hghfd', 'UHQIqEADkO.jpg', '0', '2019-12-22 06:36:37', '2019-12-22 06:46:39'),
(9, 'ljkhjgh', '1', '876543', 'rtr@3rt.fd', 'hghfd', '53DyeLtY0s.jpg', '0', '2019-12-22 06:36:37', '2019-12-22 06:46:38'),
(10, 'jhgfr', '2', '675432', 'hgfds', 'ghfdds', NULL, '0', '2019-12-22 06:42:22', '2019-12-22 06:46:37'),
(11, 'kuyjhg', '2', '56543432', 'kjmhjngbf@ghj.fgdfd', 'yjhtgrfe', 'qEqjG6TsfL.jpg', '0', '2019-12-22 06:46:17', '2019-12-22 06:46:41'),
(12, 'kuyjhg', '2', '56543432', 'kjmhjngbf@ghj.fgdfd', 'yjhtgrfe', '6TiTfx5wj4.jpg', '0', '2019-12-22 06:46:17', '2019-12-22 06:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateFrom` date NOT NULL,
  `dateTo` date NOT NULL,
  `totalLeave` int(11) NOT NULL,
  `stayingAddress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recomByTL_MM_PM` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recomByHeadOfDep` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recomByCEO` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recomByCOO` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_03_051041_create_users_table', 1),
(2, '2019_12_04_053558_add_api_token_to_users_table', 1),
(3, '2019_12_04_062557_alter_table_users_change_api_token', 1),
(4, '2019_12_08_050031_create_employees_table', 1),
(5, '2020_01_22_043228_create_roles_table', 2),
(6, '2020_01_22_055354_create_leaves_table', 3),
(7, '2020_01_22_062553_add_role_id_to_users', 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_bn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `name_bn`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '', '1', NULL, NULL),
(2, 'Employee', '', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `api_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Shariful Islam', 'shariful@gmail.com', '$2y$10$RxZGM32JHONxc5yGXIcgAefI7AOsEEc5JVAO6I8yDoY/Nis82amvy', 'c48zwM3p5tDEWIOTcaleQCnWnQcfijmDJ2rsKOe62YbcrOIVL4', 1, '2019-12-19 08:38:26', '2020-01-08 04:56:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
