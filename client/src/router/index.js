import Vue from 'vue'
// import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Home from '@/components/Home'
import Employee from '@/components/Employee'
import Slider from '@/components/Slider'
import Leave from '@/components/Leave'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      redirect:{path: '/login'}
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      children:[{
        path: '/home/employee',
        name: 'Home.Employee',
        component: Employee,
      },
      {
        path: '/',
        name: 'Home.Slider',
        component: Slider,
      },
      {
        path: '/home/leave',
        name: 'Home.Leave',
        component: Leave,
      }]
    }
  ]
})
