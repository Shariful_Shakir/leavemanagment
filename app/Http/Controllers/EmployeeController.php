<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Employee;

class EmployeeController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function getEmployee()
    {
        $employees =  Employee::where('status', 1)->get(); 
        if ($employees) {
            return response()->json(['status' => 'success', 'employees' => $employees], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Employee Not Found'], 401);
    }

    function data_uri($file, $mime) 
    {  
        $contents = file_get_contents($file);
        $base64   = base64_encode($contents); 
        return ('data:' . $mime . ';base64,' . $base64);
    }

    function getImageName($image)
    {
        //$image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'jpeg';
        return $imageName;
    }

    public function addEmployee(Request $request)
    {
        $emp = new Employee();

        $emp->name = $request->name;
        $emp->designation = $request->designation;
        $emp->email = $request->email;
        $emp->contact_no = $request->contact_no;
        $emp->address = $request->address;
        //$ext = pathinfo($request->image, PATHINFO_EXTENSION);
        $emp->status = 1;
        if($request->image){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.'jpg';

            File::put(storage_path(). '/' . $imageName, base64_decode($image));
            $emp->image = $imageName;
        }
        
        if($emp->save()){
            return response()->json(['status' => 'success', 'employee' => $emp], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Employee Not Save !'], 401);
    }

    public function updateEmployee(Request $request)
    {
        $emp = Employee::find($request->id);

        $emp->name = $request->name;
        $emp->designation = $request->designation;
        $emp->email = $request->email;
        $emp->contact_no = $request->contact_no;
        $emp->address = $request->address;

        if($request->image){
            $image = $request->image;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.'jpg';

            File::put(storage_path(). '/' . $imageName, base64_decode($image));
            $emp->image = $imageName;
        }

        if($emp->save()){
            return response()->json(['status' => 'success', 'employee' => $emp], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Employee Not Update !'], 401);
    }

    public function deleteEmployee(Request $request)
    {
        $emp = Employee::find($request->id);
        $emp->status = 0;
        if($emp->save()){
            return response()->json(['status' => 'success', 'employee' => $emp], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Employee Not Deleted !'], 401);
    }
}
