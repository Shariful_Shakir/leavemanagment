<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = app('hash')->make($request->password);
        $user->api_token = str_random(50);
        $user->status = 1;
        if ($user->save()) {
            return response()->json(['user' => $user], 200);
        }
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'User Not Found'], 401);
        }

        if (Hash::check($request->password, $user->password)) {
            $user->update(['api_token' => str_random(50)]);
            return response()->json(['status' => 'success', 'user' => $user], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Invalid Credentials'], 401);

    }

    public function logout(Request $request)
    {
        $api_token = $request->api_token;
        $user = User::where('api_token', $api_token)->first();
        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'Not Logged In'], 401);
        }
        $user->api_token = null;
        $user->save();
        return response()->json(['status' => 'success', 'message' => 'You are now logged out'], 200);
    }
}
