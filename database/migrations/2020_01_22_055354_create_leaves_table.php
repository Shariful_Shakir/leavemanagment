<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('designation');
            $table->string('contact_no');
            $table->date('dateFrom');
            $table->date('dateTo');
            $table->integer('totalLeave');
            $table->string('stayingAddress');
            $table->string('recomByTL_MM_PM');
            $table->string('recomByHeadOfDep');
            $table->string('recomByCEO');
            $table->string('recomByCOO');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
